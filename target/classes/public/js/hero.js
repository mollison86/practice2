$(document).ready(function() {
        $("#createhero").click(function() {

            var hero = {};

            hero.heroName = $("#heroname").val();
            hero.mutantName = $("#mutantname").val();
            hero.originType = $("#origintype").val();
            hero.costumeImage = $("#costumeimage").val();
            hero.height = $("#height").val();
            hero.weight = $("#weight").val();
            hero.powers = [];
            hero.powers.push({"id": $("#powers1").val()});
            alert(JSON.stringify(hero));
            $.ajax({
                url : '/heros',
                method : 'POST',
                data : JSON.stringify(hero),
                contentType : 'application/json'
            }).then(function() {
            	listHeroTable();
            	listHeros();
            }, function(errors) {
                $("#errorMsgs").text(errors);
            });

        });
       function listPowers(){
                    $.ajax({
                                url : '/powers',
                                method : 'GET'
                            })
                            .then(
                                    function(powers) {
                                        for (var i = 0; i < powers.length; i++) {
                                            var power = powers[i];
                                            var row = '<option value="' + power.id + '">'
                                                    + power.id
                                                    + ', '
                                                    + power.name
                                                    + '</option>';
                                            $("#powers1, #powers2").append(row);
                                        }
                                    });
       }
       listPowers();

                    $("#removeBtn").click(function() {
                        $.ajax({
                            url : '/heros/' + $("#removehero").val(),
                            method : 'DELETE'
                        }).then(function() {
                        	listHeroTable();
                        	listHeros();
                        }, function(errors) {

                        });
                    });
                function listHeroTable(){
                	 $("#result").children().remove();
                    $.ajax({
                        url : '/heros',
                        method : 'GET'
                    }).then(
                            function(heros) {
                                for (var i = 0; i < heros.length; i++) {
                                    var hero = heros[i];
                                    var powers = " "
                                    for(j=0; j<hero.powers.length;j++){
                                    	powers += hero.powers[j].name
                                    	powers += ', ';
                                    }
                                    var row = "<tr><td>" + hero.id
                                            + "</td><td>" + hero.heroName
                                            + "</td><td>" + hero.mutantName
                                            + "</td><td>" + hero.originType 
                                            + "</td><td>" + hero.height
                                            + "</td><td>" + hero.weight
                                            + "</td><td>" + powers
                                    		+ "</td><td>" + hero.costumeImage 
                                            + "</td></tr>";
                                    $("#result").append(row);
                                }
                            });
                }
                listHeroTable();
                function listHeros(){
                	 $("#removehero, #heros").children().remove();
                    $.ajax({
                            url: '/heros',
                            method: 'GET'
                        })
                        .then(
                            function(heros) {
                                for (var i = 0; i < heros.length; i++) {
                                    var hero = heros[i];
                                    var row = '<option value="' + hero.id + '">' + hero.heroName + ', ' + hero.mutantName + '</option>';
                                    $("#removehero, #heros").append(row);
                                }
                            });
                }
                listHeros();
                
                    $("#heros").change(function(){
                    	$("#heroid").val($("#heros").val());
                    });
                    $("#updatehero").click(function() {
                        var hero = {};
                        hero.id = $("#heroid").val();
                        hero.heroName = $("#heronameupdate").val();
                        power.mutantName = $("#mutantnameupdate").val();
                        hero.originType = $("#origintypeupdate").val();
                        power.height = $("#heightupdate").val();
                        hero.weight = $("#weightupdate").val();
                        hero.costumeImage = $("#costumeimageupdate").val();
                        hero.powers = [];
                        hero.powers.push({"id": $("#powers2").val()});
                        alert(JSON.stringify(hero));
                      
                        $.ajax({
                            url: '/powers/' + hero.id,
                            method: 'PUT',
                            data: JSON.stringify(hero),
                            contentType: 'application/JSON'
                        }).then(function() {
                        	listHeroTable();
                        	listHeros();
                        }, function(errors) {

                        });
                    });

    });