 $(document).ready(function() {
        $("#createpower").click(function() {

            var power = {};

            power.name = $("#name").val();
            power.type = {};
            power.type.id = $("#powertypes1").val();
            power.description = $("#description").val();

            $.ajax({
                url : '/powers',
                method : 'POST',
                data : JSON.stringify(power),
                contentType : 'application/json'
            }).then(function() {
            	listPowerTable();
            	listPowers();
            }, function(errors) {
                $("#errorMsgs").text(errors);
            });

        });
       function listPowerTypes(){
                    $.ajax({
                                url : '/powertypes',
                                method : 'GET'
                            })
                            .then(
                                    function(powertypes) {
                                        for (var i = 0; i < powertypes.length; i++) {
                                            var type = powertypes[i];
                                            var row = '<option value="' + type.id + '">'
                                                    + type.id
                                                    + ', '
                                                    + type.type
                                                    + '</option>';
                                            $("#powertypes1, #powertypes2").append(row);
                                        }
                                    });
       }
       listPowerTypes();

                    $("#removeBtn").click(function() {
                        $.ajax({
                            url : '/powers/' + $("#removepower").val(),
                            method : 'DELETE'
                        }).then(function() {
                        	listPowerTable();
                        	listPowers();
                        }, function(errors) {

                        });
                    });
                function listPowerTable(){
                	 $("#result").children().remove();
                    $.ajax({
                        url : '/powers',
                        method : 'GET'
                    }).then(
                            function(powers) {
                                for (var i = 0; i < powers.length; i++) {
                                    var power = powers[i];
                                    var row = "<tr><td>" + power.id
                                            + "</td><td>" + power.name
                                            + "</td><td>" + power.type.type
                                            + "</td><td>" + power.description 
                                            + "</td></tr>";
                                    $("#result").append(row);
                                }
                            });
                }
                listPowerTable();
                function listPowers(){
                	 $("#removepower, #powers").children().remove();
                    $.ajax({
                            url: '/powers',
                            method: 'GET'
                        })
                        .then(
                            function(powers) {
                                for (var i = 0; i < powers.length; i++) {
                                    var power = powers[i];
                                    var row = '<option value="' + power.id + '">' + power.name + ', ' + power.type.type + '</option>';
                                    $("#removepower,#powers").append(row);
                                }
                            });
                }
                listPowers();
                
                    $("#powers").change(function(){
                    	$("#powerId").val($("#powers").val());
                    });
                    $("#updatepower").click(function() {
                        var power = {};
                        power.id = $("#powerId").val();
                        power.name = $("#updatename").val();
                        power.description = $("#updatedescription").val();
                        power.type = {};
                        power.type.id =	$("#powertypes2").val();
                      
                        $.ajax({
                            url: '/powers/' + power.id,
                            method: 'PUT',
                            data: JSON.stringify(power),
                            contentType: 'application/JSON'
                        }).then(function() {
                        	listPowerTable();
                        	listPowers();
                        }, function(errors) {

                        });
                    });
             $("#powerId").val = $("#powers").val();

    });