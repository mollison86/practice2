package superheroTeams.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import superheroTeams.entities.Power;
import superheroTeams.entities.PowerType;
import superheroTeams.entities.Superhero;
import superheroTeams.entities.SuperheroTeam;
@Repository
@Transactional
public class SuperheroDaoImpl implements SuperheroDao{
	
	@PersistenceContext
	private EntityManager em;
	
	
	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<SuperheroTeam> getTeams() {
		return  em.createQuery("SELECT t FROM SuperheroTeam t", SuperheroTeam.class).getResultList();
	}

	@Override
	public SuperheroTeam getTeam(int id) {
		return em
				.createQuery("SELECT t FROM SuperheroTeam t WHERE t.id = :id", SuperheroTeam.class)
				.setParameter("id",  id)
				.getSingleResult();
	}

	@Override
	public void addTeam(SuperheroTeam team) {
		em.merge(team);
		
	}

	@Override
	public void deleteTeam(int id) {
		SuperheroTeam team = em
				.createQuery("SELECT t FROM SuperheroTeam t WHERE t.id = :id", SuperheroTeam.class)
				.setParameter("id", id)
				.getSingleResult();
				em.remove(team);	
		
	}

	@Override
	public void updateTeam(SuperheroTeam team) {
		em.merge(team);
	}

	@Override
	public List<Superhero> getHeros() {
		return em.createQuery("SELECT s FROM Superhero s", Superhero.class).getResultList();
	}

	@Override
	public Superhero getHero(int id) {
		return em
				.createQuery("SELECT s FROM Superhero s WHERE s.id = :id", Superhero.class)
				.setParameter("id",  id)
				.getSingleResult();
	}

	@Override
	public void addHero(Superhero hero) {
		em.merge(hero);
	}

	@Override
	public void deleteHero(int id) {
		Superhero hero = em
				.createQuery("SELECT s FROM Superhero s WHERE s.id = :id", Superhero.class)
				.setParameter("id", id)
				.getSingleResult();
				em.remove(hero);			
	}

	@Override
	public void updateHero(Superhero hero) {
		em.merge(hero);
	}

	@Override
	public List<Power> getPowers() {
		return em.createQuery("SELECT p FROM Power p", Power.class).getResultList();
	}

	@Override
	public Power getPower(int id) {
		return em
				.createQuery("SELECT p FROM Power p WHERE p.id = :id", Power.class)
				.setParameter("id",  id)
				.getSingleResult();
	}

	@Override
	public void addPower(Power power) {
		int id = power.getType().getId();
        String type = em.createQuery("SELECT p.type.type FROM Power p WHERE p.type.id = :id", String.class).setParameter("id", id).getSingleResult();
		power.getType().setType(type);
        em.merge(power);
	}

	@Override
	public void deletePower(int id) {
		Power power = em
				.createQuery("SELECT p FROM Power p WHERE p.id = :id", Power.class)
				.setParameter("id", id)
				.getSingleResult();
				em.remove(power);			
	}

	@Override
	public void updatePower(Power power) {
		em.merge(power);
	}

	@Override
	public List<PowerType> getPowerTypes() {
		 return em.createQuery("SELECT p FROM PowerType p", PowerType.class).getResultList();
	}

}
