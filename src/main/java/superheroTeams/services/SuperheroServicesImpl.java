package superheroTeams.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import superheroTeams.dao.SuperheroDao;
import superheroTeams.entities.Power;
import superheroTeams.entities.PowerType;
import superheroTeams.entities.Superhero;
import superheroTeams.entities.SuperheroTeam;
	@Service
public class SuperheroServicesImpl implements SuperheroServices{
	
	@Autowired
	SuperheroDao dao;

	public void setDao(SuperheroDao dao) {
		this.dao = dao;
	}

	@Override
	public List<SuperheroTeam> getTeams() {
		return dao.getTeams();
	}

	@Override
	public SuperheroTeam getTeam(int id) {
		return dao.getTeam(id);
	}

	@Override
	public void addTeam(SuperheroTeam team) {
		dao.addTeam(team);
	}

	@Override
	public void deleteTeam(int id) {
		dao.deleteTeam(id);
	}

	@Override
	public void updateTeam(SuperheroTeam team) {
		dao.updateTeam(team);
	}

	@Override
	public List<Superhero> getHeros() {
		return dao.getHeros();
	}

	@Override
	public Superhero getHero(int id) {
		return dao.getHero(id);
	}

	@Override
	public void addHero(Superhero hero) {
		dao.addHero(hero);
	}

	@Override
	public void deleteHero(int id) {
		dao.deleteHero(id);
	}

	@Override
	public void updateHero(Superhero hero) {
		dao.updateHero(hero);
	}

	@Override
	public List<Power> getPowers() {
		return dao.getPowers();
	}

	@Override
	public Power getPower(int id) {
		return dao.getPower(id);
	}

	@Override
	public void addPower(Power power) {
		dao.addPower(power);
	}

	@Override
	public void deletePower(int id) {
		dao.deletePower(id);
	}

	@Override
	public void updatePower(Power power) {
		dao.updatePower(power);
	}

	@Override
	public List<PowerType> getPowerTypes() {
		return dao.getPowerTypes();
	}

}
